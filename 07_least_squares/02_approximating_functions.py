#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt


t = np.linspace(0, np.pi / 2)
y = np.sin(t)

# Constant

alpha = 2 / np.pi
y_const = alpha * np.ones(np.shape(y))

# Linear

A = np.array([[np.pi / 2, np.pi**2 / 8], [np.pi**2 / 8, np.pi**3 / 24]])
b = np.array([1, 1])
coeff_lin = np.linalg.solve(A, b)
y_lin = coeff_lin[0] + coeff_lin[1] * t

# Quadratic

A = np.array([[np.pi / 2, np.pi**2 / 8, np.pi**3 / 24],
              [np.pi**2 / 8, np.pi**3 / 24, np.pi**4 / 64],
              [np.pi**3 / 24, np.pi**4 / 64, np.pi**5 / 160]])
b = np.array([1, 1, np.pi - 2])
coeff_sq = np.linalg.solve(A, b)
y_sq = coeff_sq[0] + coeff_sq[1] * t + coeff_sq[2] * t**2

# Plot

plt.plot(t, y)
plt.plot(t, y_const)
plt.plot(t, y_lin)
plt.plot(t, y_sq)
plt.legend(('exact', 'constant', 'linear', 'quadratic'))
plt.show()
