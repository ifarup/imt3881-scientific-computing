#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt


time = np.arange(1, 11)
years = 1990 + time
temperature = np.array([.29, .14, .19, .26, .38, .22, .43, .59, .33, .29])

# Constant

alpha = temperature.mean()
temp_const = alpha * np.ones(np.shape(temperature))

# Linear

N = np.shape(temperature)[0]
t1sum = time.sum()
t2sum = (time**2).sum()
ysum = temperature.sum()
ytsum = (temperature * time).sum()

A = np.array([[N, t1sum], [t1sum, t2sum]])
b = np.array([ysum, ytsum])
coeff = np.linalg.solve(A, b)
temp_lin = coeff[0] + coeff[1] * time

# Quadratic

t3sum = (time**3).sum()
t4sum = (time**4).sum()
yt2sum = (temperature * time**2).sum()

A = np.array([[N, t1sum, t2sum], [t1sum, t2sum, t3sum], [t2sum, t3sum, t4sum]])
b = np.array([ysum, ytsum, yt2sum])
coeff = np.linalg.solve(A, b)
temp_sq = coeff[0] + coeff[1] * time + coeff[2] * time**2

# Plot

plt.plot(years, temperature, 'o')
plt.plot(years, temp_const)
plt.plot(years, temp_lin)
plt.plot(years, temp_sq)
plt.show()
