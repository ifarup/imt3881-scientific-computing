#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize

time = np.arange(1, 11)
years = 1990 + time
temperature = np.array([.29, .14, .19, .26, .38, .22, .43, .59, .33, .29])


# Constant

def f_const(x, a):
    return a * np.ones(np.shape(x))

guess = .5
params, params_covariance = scipy.optimize.curve_fit(f_const, time,
                                                     temperature, guess)
temp_const = params[0] * np.ones(np.shape(temperature))


# Linear

def f_lin(x, a, b):
    return a + b * x

guess = [.5, .5]
params, params_covariance = scipy.optimize.curve_fit(f_lin, time,
                                                     temperature, guess)
temp_lin = params[0] + params[1] * time


# Quadratic

def f_sq(x, a, b, c):
    return a + b * x + c * x**2

guess = [.5, .5, .5]
params, params_covariance = scipy.optimize.curve_fit(f_sq, time,
                                                     temperature, guess)
temp_sq = params[0] + params[1] * time + params[2] * time**2

# Plot

plt.plot(years, temperature, 'o')
plt.plot(years, temp_const)
plt.plot(years, temp_lin)
plt.plot(years, temp_sq)
plt.show()
