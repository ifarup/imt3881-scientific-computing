import numpy as np
import matplotlib.pyplot as plt

years = np.arange(1, 6)
price = np.array([55.2, 44.9, 37.9, 35.3, 30.1])

# Build linear model

N = np.shape(price)[0]
t1sum = years.sum()
t2sum = (years**2).sum()
ysum = price.sum()
ytsum = (price * years).sum()

A = np.array([[N, t1sum], [t1sum, t2sum]])
b = np.array([ysum, ytsum])
coeff = np.linalg.solve(A, b)
linear_model = coeff[0] + coeff[1] * years

# Build exponential model

log_price = np.log(price)
ysum = log_price.sum()
ytsum = (log_price * years).sum()

A = np.array([[N, t1sum], [t1sum, t2sum]])
b = np.array([ysum, ytsum])
coeff = np.linalg.solve(A, b)
exp_model = np.exp(coeff[0] + coeff[1] * years)

# Plot results

plt.plot(years, price)
plt.plot(years, linear_model)
plt.plot(years, exp_model)
plt.show()