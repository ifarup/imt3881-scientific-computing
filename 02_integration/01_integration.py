#!/usr/bin/env python3

import unittest
import numpy as np
from scipy.integrate import quad


def one_trapezoid(f, a, b):
    """
    Numerical integration approximation with one trapezoid (ESC 1.3.1)
    """
    return 0.5 * (b - a) * (f(a) + f(b))


def two_trapezoids(f, a, b):
    """
    Numerical integration approximation with two trapezoids (ESC 1.3.2)
    """
    c = .5 * (a + b)            # mid point
    return 0.25 * (b - a) * (f(a) + 2 * f(c) + f(b))


def n_trapezoids(f, a, b, n):
    """
    Numerical integration approximation with n trapezoids (ESC 1.3.3)
    """
    h = (b - a) / n
    x_points = np.linspace(a, b, n + 1)
    return h * (0.5 * f(x_points[0]) +
                0.5 * f(x_points[-1]) +
                f(x_points[1:-1]).sum())


# Tests

class test_integration(unittest.TestCase):

    def test_one_trapezoid(self):
        one_t = one_trapezoid(lambda x: x, 0, 1)
        self.assertTrue(np.abs(one_t - .5) < 1e-15)

    def test_two_trapezoids(self):
        two_t = two_trapezoids(lambda x: x, 0, 1)
        self.assertTrue(np.abs(two_t - .5) < 1e-15)

    def test_n_trapezoids(self):
        n_t = n_trapezoids(lambda x: x, 0, 1, 50)
        self.assertTrue(np.abs(n_t - .5) < 1e-15)


# Main

def main():

    def f(x):
        """
        Bagel and Juice function to be integrated
        """
        return np.exp(- (x - 300)**2 / (2 * 20**2)) / (20 * np.sqrt(2 * np.pi))

    def f_errortesting(x):
        """
        Function for testing the error, ESC 1.4, Table 1.1
        """
        return (1 + x) * np.exp(x)

    print()
    print('   1 trapezoid : ' + str(one_trapezoid(f, 300, 330)))
    print('   2 trapezoids: ' + str(two_trapezoids(f, 300, 330)))
    print('  10 trapezoids: ' + str(n_trapezoids(f, 300, 330, 10)))
    print(' 100 trapezoids: ' + str(n_trapezoids(f, 300, 330, 100)))
    print('1000 trapezoids: ' + str(n_trapezoids(f, 300, 330, 1000)))
    print('scipy.quad     : ' + str(quad(f, 300, 330)[0]))

    # Compute and print the error table (ESC Table 1.1)

    print('\nn\th\t\tE_h\t\tE_h/h^2')
    for n in [2**k for k in range(10)]:
        h = 1 / n
        E_h = np.abs(np.e - n_trapezoids(f_errortesting, 0, 1, n))
        print('%d\t%f\t%f\t%f' % (n, h, E_h, E_h / h**2))

    # Compute the number of bagels and the corresponding p value (ESC
    # Table 1.3)

    print('\nb\tp')
    for b in range(331, 335):
        print('%d\t%.3f' % (b, 0.5 + n_trapezoids(f, 300, b, 100)))


if __name__ == '__main__':
    main()
