#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

# Parameters

dt = 1/1000
F0 = 1.9
S0 = 0.1
T = 20

# Set up

N = int(T / dt + 1)
t = np.linspace(0, T, N)
F = np.zeros(np.shape(t))
S = np.zeros(np.shape(t))
F[0] = F0
S[0] = S0

# Compute

for i in range(N - 1):
    F[i + 1] = ((1 - dt**2 / 4) * F[i] + dt * (dt / 2 + 1) -
                dt * S[i]) / (1 + dt**2 / 4)
    S[i + 1] = ((1 - dt**2 / 4) * S[i] + dt * (dt / 2 - 1) +
                dt * F[i]) / (1 + dt**2 / 4)

# Plot

plt.figure()
plt.plot(t, F, t, S)
plt.xlabel('t')
plt.ylabel('population of fish & shark')
plt.legend(('fish', 'shark'))

plt.figure()
plt.plot(F, S)
plt.xlabel('F')
plt.ylabel('S')
plt.axis('equal')
plt.show()
