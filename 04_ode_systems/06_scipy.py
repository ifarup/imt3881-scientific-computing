#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint


def limited(y, t):
    """
    Right hand side of the ode system for growth with limited resources
    """
    return [(2 - y[0] - y[1]) * y[0],
            (y[0] - 1) * y[1]]

# Parameters

F0 = 1.9
S0 = 0.1
T = 20

# Compute

t = np.linspace(0, T)
y = odeint(limited, [F0, S0], t)

# Plot

plt.figure()
plt.plot(t, y)
plt.xlabel('t')
plt.ylabel('population of fish & shark')
plt.legend(('fish', 'shark'))
plt.show()
