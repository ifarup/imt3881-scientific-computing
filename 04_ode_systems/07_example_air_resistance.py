import numpy as np
import matplotlib.pyplot as plt

v0 = 10
theta = np.pi / 4
g = 9.81

def explicit(T, N, k=0):
    dt = T / N

    x = np.zeros(N + 1)
    y = np.zeros(N + 1)
    vx = np.zeros(N + 1)
    vy = np.zeros(N + 1)

    x[0] = 0
    y[0] = 0
    vx[0] = v0 * np.cos(theta)
    vy[0] = v0 * np.sin(theta)

    for i in range(N):
        x[i + 1] = x[i] + dt * vx[i]
        y[i + 1] = y[i] + dt * vy[i]
        vx[i + 1] = vx[i] - dt * k * np.sqrt(vx[i]**2 + vy[i]**2) * vx[i]
        vy[i + 1] = vy[i] - dt * (k * np.sqrt(vx[i]**2 + vy[i]**2) * vy[i] + g)

    return x, y, vx, vy

plt.clf()

for k in np.linspace(0, .3, 5):
    x, y, vx, vy = explicit(1.5, 100, k)
    plt.plot(x, y)

plt.show()
