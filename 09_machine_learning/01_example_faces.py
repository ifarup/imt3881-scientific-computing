import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets, decomposition, svm

plt.ion()

people = datasets.fetch_lfw_people(min_faces_per_person=50, resize=.4)

# plt.imshow(people.images[0])
plt.imshow(people.data[0].reshape(50, 37))
plt.show()
plt.pause(1)

pca = decomposition.PCA(n_components=150, whiten=False,
                        svd_solver='randomized')
pca.fit(people.data)

people_pca = pca.transform(people.data)

plt.imshow(np.dot(people_pca[0], pca.components_).reshape(50, 37))
plt.show()
plt.pause(1)

clf = svm.SVC(C=5., gamma=0.001)
clf.fit(people_pca[::2], people.target[::2])

corr = 0
for i in range(100):
    index = clf.predict(people_pca[i].reshape(1, -1))[0]
    if people.target_names[index] == people.target_names[people.target[i]]:
        corr += 1
    print(people.target_names[index],
          people.target_names[people.target[i]])
print("Correct: " + str(corr))
    
for i in range(150):
    plt.imshow(pca.components_[i].reshape(50, 37))
    plt.show()
    plt.pause(.5)
