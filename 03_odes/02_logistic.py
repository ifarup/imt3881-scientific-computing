#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import unittest


def explicit(n, t, y0):
    """
    Solve the logistic equation explicitly.

    Solve the logistic equation y' = y * (1 - y) with the initial
    value y(0) = y0 from 0 to t using n steps with explicit
    integration.
    """
    dt = t / n
    y_values = np.zeros(n + 1)  # allocate the space
    y_values[0] = y0
    for i in range(n):
        y_values[i + 1] = y_values[i] + dt * y_values[i] * (1 - y_values[i])
    return y_values


def implicit(n, t, y0):
    """
    Solve the logistic equation explicitly.

    Solve the logistic equation y' = y * (1 - y) with the initial
    value y(0) = y0 from 0 to t using n steps with explicit
    integration.
    """
    dt = t / n
    y_values = np.zeros(n + 1)  # allocate the space
    y_values[0] = y0
    for i in range(n):
        y_values[i + 1] = (-1 + dt +
                           np.sqrt((1 - dt)**2 +
                                   4 * dt * y_values[i])) / (2 * dt)
    return y_values


# Tests

class test_logistic(unittest.TestCase):

    def test_explicit(self):
        y = explicit(1, 1, 1)
        self.assertTrue(np.abs(y[-1] - 1) < 1e-15)

    def test_implicit(self):
        y = implicit(1, 1, 1)
        self.assertTrue(np.abs(y[-1] - 1) < 1e-15)


# Main

if __name__ == '__main__':

    # Generate plot with explicit and implicit integration

    plt.figure()
    plt.title('Logistic equation')
    plt.plot(explicit(100, 10, .2))
    plt.plot(implicit(100, 10, .2))
    plt.legend(('explicit', 'implicit'))

    plt.show()
