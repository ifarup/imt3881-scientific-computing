#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint


def logistic(y, t):
    """
    Right hand side of logistic equation
    """
    return y * (1 - y)

# Compute

t = np.linspace(0, 10)
y = odeint(logistic, .2, t)

# Plot

plt.plot(t, y)
plt.show()
