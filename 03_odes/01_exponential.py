#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import unittest


def explicit(n):
    """
    Solve the equation y' = -100 y, y(0) = 1.

    Solve the equation from 0 to 1 using N steps with explicit
    integration.
    """
    n_values = np.arange(n + 1)
    y_values = (1 - 100 / n)**n_values
    return y_values


def implicit(n):
    """
    Solve the equation y' = -100 y, y(0) = 1.

    Solve the equation from 0 to 1 using N steps with implicit
    integration.
    """
    n_values = np.arange(n + 1)
    y_values = (n / (n + 100))**n_values
    return y_values


# Tests

class test_exponential(unittest.TestCase):

    def test_explicit(self):
        y = explicit(1)
        self.assertTrue(np.abs(y[-1] + 99) < 1e-15)

    def test_implicit(self):
        y = implicit(1)
        self.assertTrue(np.abs(y[-1] - .0099) < 1e-5)


# Main

if __name__ == '__main__':

    # Generate plot with explicit integration

    plt.figure()
    plt.subplot(2, 1, 1)
    plt.title('Explicit integration')
    plt.plot(explicit(50))
    plt.subplot(2, 1, 2)
    plt.plot(explicit(200))

    # Generate plot with implicit integration

    plt.figure()
    plt.subplot(2, 1, 1)
    plt.title('Implicit integration')
    plt.plot(implicit(10), '*')
    plt.subplot(2, 1, 2)
    plt.plot(implicit(200), '*')

    plt.show()
