#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import unittest


def explicit(f, n, t0, tend, y0):
    """
    Solve the general ode explicitly.

    Solves y' = f(y) from t0 to tend in n steps with y(t0) = y0.
    """
    dt = (tend - t0) / n
    y_values = np.zeros(n + 1)  # allocate the space
    y_values[0] = y0
    for i in range(n):
        y_values[i + 1] = y_values[i] + dt * f(y_values[i])
    return y_values


def heun(f, n, t0, tend, y0):
    """
    Solve the general ode implicitly.

    Solves y' = f(y) from t0 to tend in n steps with y(t0) = y0.
    """
    dt = (tend - t0) / n
    y_values = np.zeros(n + 1)  # allocate the space
    y_values[0] = y0
    for i in range(n):
        y_values[i + 1] = y_values[i] + \
                          .5 * dt * (f(y_values[i]) +
                                     f(y_values[i] +
                                       dt * f(y_values[i])))
    return y_values


# Tests

class test_general(unittest.TestCase):

    def test_explicit(self):
        y = explicit(lambda x: x, 1, 0, 1, 1)
        self.assertTrue(np.abs(y[-1] - 2) < 1e-15)

    def test_heun(self):
        y = heun(lambda x: x, 1, 0, 1, 1)
        self.assertTrue(np.abs(y[-1] - 2.5) < 1e-15)


# Main

if __name__ == '__main__':

    # Generate plot with explicit and heun integration

    plt.figure()
    plt.title('Exponential decay')
    t = np.linspace(0, 1, 11)
    plt.plot(t, explicit(lambda x: -x, 10, 0, 1, 1))
    plt.plot(t, heun(lambda x: -x, 10, 0, 1, 1))
    plt.plot(t, np.exp(-t))
    plt.legend(('explicit', 'heun', 'exact'))

    plt.show()
