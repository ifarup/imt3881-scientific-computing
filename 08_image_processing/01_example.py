import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage

im = plt.imread('Fun_Dots_Room__46902_zoom.jpg').sum(2).astype(float) / (3 * 255)
bin = im < .8
open_im = ndimage.binary_opening(bin, iterations=10)
label_im, n_labels = ndimage.label(open_im)
sizes = []
for i in range(1, n_labels + 1):
    sizes.append((label_im == i).sum())
hist, edges = np.histogram(sizes)
hist = hist.tolist()
hist.append(0)
print(hist, edges)
plt.plot(edges, hist)
plt.show()
