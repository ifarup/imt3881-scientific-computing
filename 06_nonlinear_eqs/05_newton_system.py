#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize


def newton(func, jac, x, tol=1e-5, debug=False):
    """
    Solve func(x) = 0 for x (starting at x) with Newton's method
    """
    while(abs(func(x)).sum() > tol):
        if debug:
            print(x)
        x = x - np.dot(np.linalg.inv(jac(x)), func(x))
    return x


def f(x):
    """
    Test function for finding root
    """
    return np.array([np.exp(x[0]) - np.exp(x[1]),
                     np.log(1 + x[0] + x[1])])


def f_jac(x):
    """
    Jacobian of test function
    """
    return np.array([[np.exp(x[0]), -np.exp(x[1])],
                     [1 / (1 + x[0] + x[1]),
                      1 / (1 + x[0] + x[1])]])


# Test

print(newton(f, f_jac, [.7, -.2], debug=True))
print()
print(scipy.optimize.root(f, [.5, .5], jac=f_jac))  # not necessarily Newton
