#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize


def fixed_point(func, x0, tol=1e-5, debug=False):
    """
    Find fixed-point, x = func(x), by fixed-point iteration
    """
    x = func(x0)
    while(abs(x - x0) > tol):
        if debug:
            print(x)
        x0 = x
        x = func(x)
    return x


def f(x):
    """
    Test function for finding fixed-point
    """
    return np.sin(x / 10)


# Test

print(fixed_point(f, 1, debug=True))
print()
print(scipy.optimize.fixed_point(f, 1,))
