import numpy as np
import matplotlib.pyplot as plt

def explicit(T, N):
    dt = T / N
    u = np.zeros(N + 1)
    for n in range(N):
        u[n + 1] = u[n] + dt * np.exp(-u[n])
    return u

def newton(func, deriv, x, tol=1e-10, debug=False):
    """
    Solve func(x) = 0 for x (starting at x) with Newton's method
    """
    while(abs(func(x)) > tol):
        if debug:
            print(x)
        x = x - func(x) / deriv(x)
    return x

def implicit(T, N):
    dt = T / N
    u = np.zeros(N + 1)
    for n in range(N):
        u[n + 1] = newton(lambda unp1: unp1 - dt * np.exp(-unp1) - u[n],
                          lambda unp1: 1 + dt * np.exp(-unp1), u[n])
    return u

def crank_nicolson(T, N):
    dt = T / N
    u = np.zeros(N + 1)
    for n in range(N):
        u[n + 1] = newton(lambda unp1: unp1 - .5 * dt * np.exp(-unp1) - u[n] -
                          .5 * dt * np.exp(-u[n]),
                          lambda unp1: 1 + .5 * dt * np.exp(-unp1), u[n])
    return u

def innovative(T, N):
    dt = T / N
    u = np.zeros(N + 1)
    for n in range(N):
        u[n + 1] = newton(lambda unp1: unp1 - dt * np.exp(-.5 * (unp1 + u[n])) - u[n],
                          lambda unp1: 1 + .5 * dt * np.exp(-.5 * (unp1 + u[n])), u[n])
    return u

T = 1
N = 100

u_explicit = explicit(T, N)
u_implicit = implicit(T, N)
u_cn = crank_nicolson(T, N)
u_innovative = innovative(T, N)
u_exact = np.log(1 + np.linspace(0, 1, N + 1))

print(np.abs(u_explicit[N] - u_exact[N]))
print(np.abs(u_implicit[N] - u_exact[N]))
print(np.abs(u_cn[N] - u_exact[N]))
print(np.abs(u_innovative[N] - u_exact[N]))

plt.clf()
plt.plot(u_explicit)
plt.plot(u_implicit)
plt.plot(u_cn)
plt.plot(u_innovative)
plt.plot(u_exact)
plt.show()
