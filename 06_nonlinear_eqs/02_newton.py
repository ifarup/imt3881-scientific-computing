#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize


def newton(func, deriv, x, tol=1e-5, debug=False):
    """
    Solve func(x) = 0 for x (starting at x) with Newton's method
    """
    while(abs(func(x)) > tol):
        if debug:
            print(x)
        x = x - func(x) / deriv(x)
    return x


def f(x):
    """
    Test function for finding root
    """
    return 2 + x - np.exp(x)


def df_dx(x):
    """
    Derivative of test function
    """
    return 1 - np.exp(x)


# Test

print(newton(f, df_dx, 3, debug=True))
print()
print(scipy.optimize.newton(f, 3, df_dx))
