#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt


def secant(func, x0, x1, tol=1e-5, debug=False):
    """
    Solve func(x) = 0 for x in [a, b] with the secant method
    """
    xpp = x0                    # p for previous
    xp = x1
    x = x1
    while(abs(func(x)) > tol):
        x = xp - func(xp) * (xp - xpp) / (func(xp) - func(xpp))
        if debug:
            print(x)
        xpp = xp
        xp = x
    return x


def f(x):
    """
    Test function for finding root
    """
    return 2 + x - np.exp(x)


# Test

print(secant(f, 2, 3, debug=True))
