#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize


def bisection(func, a, b, tol=1e-5, debug=False):
    """
    Solve func(x) = 0 for x in [a, b] with the bisection method
    """
    c = .5 * (a + b)
    while(abs(a - b) > tol):
        if debug:
            print(c)
        if func(a) * func(c) < 0:
            b = c
        else:
            a = c
        c = .5 * (a + b)
    return c


def f(x):
    """
    Test function for finding root
    """
    return 2 + x - np.exp(x)


# Test

print(bisection(f, 0, 3, debug=True))
print()
print(scipy.optimize.bisect(f, 0, 3))
