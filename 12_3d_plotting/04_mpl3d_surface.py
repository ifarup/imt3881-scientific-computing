#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

x, y = np.meshgrid(np.linspace(-1, 1), np.linspace(-1, 1))
z = x**2 - y**2

ax.plot_surface(x, y, z)
plt.xlabel('x')
plt.ylabel('y')
plt.show()