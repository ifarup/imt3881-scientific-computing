#!/usr/bin/env python

import numpy as np
from mayavi import mlab

x, y = np.mgrid[-10:10:100j, -10:10:100j]
r = np.sqrt(x**2 + y**2)
z = np.sin(r)/r
mlab.surf(z, warp_scale='auto')

mlab.show()