#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

x = np.random.rand(20)
y = np.random.rand(20)
z = np.random.rand(20)

ax.scatter(x[:10], y[:10], z[:10])
ax.scatter(x[10:], y[10:], z[10:], c='r')
plt.xlabel('x')
plt.ylabel('y')
plt.show()