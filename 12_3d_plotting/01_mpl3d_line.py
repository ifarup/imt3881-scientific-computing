#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

t = np.linspace(0, 5, 500)
x = np.cos(2 * np.pi * t)
y = np.sin(2 * np.pi * t)

ax.plot(x, y, t)
ax.plot(t*x, t*y, t)
plt.xlabel('x')
plt.ylabel('y')
plt.show()
