#!/usr/bin/env python

import numpy as np
from mayavi import mlab

t = np.linspace(0, 2, 100)
mlab.plot3d(np.sin(2* np.pi * t), np.cos(2 * np.pi * t), t, t)
mlab.show()