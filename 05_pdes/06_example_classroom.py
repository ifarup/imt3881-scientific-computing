import numpy as np
import matplotlib.pyplot as plt

T = 21 * np.ones((70, 100))
T[0, 20:40] = 8
T[-10, -20] = 40
plt.ion()
T_data = plt.imshow(T)
plt.draw()

alpha = .25
while True:
    T[1:-1, 1:-1] += alpha * (T[:-2, 1:-1] +
                              T[2:, 1:-1] +
                              T[1:-1, :-2] +
                              T[1:-1, 2:] -
                              4 * T[1:-1, 1:-1])
    T[-10, -20] = 40
    T_data.set_array(T)
    plt.draw()
    plt.pause(1e-4)
