#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt


# Initialise

N_points = 50
x = np.linspace(0, 1, N_points)
T = np.ones(np.shape(x))
T[int(N_points / 2):] = 0

# Initialise for plotting

plt.ion()
line, = plt.plot(x, T)
plt.draw()

# Solve the diffusion equation

alpha = .25                     # dt / dx**2
while True:
    T[1:-1] += alpha * (T[0:-2] - 2 * T[1:-1] + T[2:])
    T[0] = T[1]
    T[-1] = T[-2]
    line.set_ydata(T)
    plt.draw()
    plt.pause(1e-4)
