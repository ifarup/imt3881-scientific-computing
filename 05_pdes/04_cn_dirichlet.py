#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import spdiags

# Initalize

N_points = 50
x = np.linspace(0, 1, N_points)
x = np.linspace(0, 1, N_points)
T = np.ones(np.shape(x))
T[int(N_points / 2):] = 0

# Initialize plot

plt.ion()
line, = plt.plot(x, T)

# Compute matrix

alpha = .5                       # dt / dx**2
upperdiag = np.concatenate(([0, 0], -.5 * alpha * np.ones(N_points - 2)))
centerdiag = np.concatenate(([1], (1 + alpha) * np.ones(N_points - 2),
                             [1]))
lowerdiag = np.concatenate((-.5 * alpha * np.ones(N_points - 2), [0, 0]))
diags = np.array([upperdiag, centerdiag, lowerdiag])
A = spdiags(diags, [1, 0, -1], N_points, N_points).todense()

upperdiag = np.concatenate(([0, 0], .5 * alpha * np.ones(N_points - 2)))
centerdiag = np.concatenate(([1], (1 - alpha) * np.ones(N_points - 2),
                             [1]))
lowerdiag = np.concatenate((.5 * alpha * np.ones(N_points - 2), [0, 0]))
diags = np.array([upperdiag, centerdiag, lowerdiag])
B = spdiags(diags, [1, 0, -1], N_points, N_points).todense()

# Solve the diffusion equation

T = np.array([T]).T

while True:
    b = np.dot(B, T)
    T = np.linalg.solve(A, b)
    line.set_ydata(T)
    plt.draw()
    plt.pause(1e-4)
