#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import spdiags

# Initalize

N_points = 50
x = np.linspace(0, 1, N_points)
T = np.ones(np.shape(x))
T[int(N_points / 2):] = 0

# Initialize plot

plt.ion()
line, = plt.plot(x, T)

# Compute matrix

alpha = 10                       # dt / dx**2
upperdiag = np.concatenate(([0, 0], -alpha * np.ones(N_points - 2)))
centerdiag = np.concatenate(([1], (1 + 2 * alpha) * np.ones(N_points - 2),
                             [1]))
lowerdiag = np.concatenate((-alpha * np.ones(N_points - 2), [0, 0]))
diags = np.array([upperdiag, centerdiag, lowerdiag])
A = spdiags(diags, [1, 0, -1], N_points, N_points).todense()

# Solve the diffusion equation

while True:
    T = np.linalg.solve(A, T)
    line.set_ydata(T)
    plt.draw()
    plt.pause(1e-4)
